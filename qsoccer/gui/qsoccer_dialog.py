import pandas as pd
from qgis.core import (
    QgsCoordinateReferenceSystem,
    QgsFeature,
    QgsField,
    QgsGeometry,
    QgsLayerTreeGroup,
    QgsLayerTreeLayer,
    QgsPointXY,
    QgsProject,
    QgsRectangle,
    QgsVectorLayer,
)
from qgis.PyQt.QtCore import QSize, QVariant
from qgis.PyQt.QtWidgets import (
    QAction,
    QButtonGroup,
    QComboBox,
    QHBoxLayout,
    QLabel,
    QRadioButton,
    QToolBar,
    QVBoxLayout,
    QWidget,
)

from qsoccer.__about__ import QGS_PROJECT_DIR
from qsoccer.toolbelt.log_handler import PlgLogger

from .soccer_plot_view import SoccerPitchWidget


class QSoccerDialog(QWidget):
    """Main QSoccer widget"""

    def __init__(self, parent, iface, dataset):
        """Create a plot dialog.

        Parameters
        ----------
        parent: QObject
        Qt parent object
        iface: QgisInterface
        QGIS interface class
        """

        super().__init__(parent)
        self.setWindowTitle("Soccer pitch")
        self.setMinimumSize(QSize(600, 400))

        # Init QGIS interface
        self.__iface = iface
        # Init dataset
        self.__dataset = dataset

        self.log = PlgLogger().log

        # Build the Widget layout
        main_layout = QVBoxLayout()
        main_layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(main_layout)
        # Add some Combo Box for determining which player one should focus on
        # Competition
        compet_layout = QHBoxLayout()
        compet_layout.addWidget(QLabel("Competition:"))
        self._competition = QComboBox()
        self._competition.setStyleSheet("QComboBox { combobox-popup: 0; }")
        self._competition.addItems(
            [
                f"{c['competition_name']} ({c['season_name']})"
                for c in dataset.competitions
            ]
        )
        self._competition.setMaxVisibleItems(10)
        self._competition.activated[str].connect(self.on_competition_changed)
        compet_layout.addWidget(self._competition)
        # Match
        main_layout.addLayout(compet_layout)
        match_layout = QHBoxLayout()
        self._match_label = QLabel("Match:")
        match_layout.addWidget(self._match_label)
        self._match = QComboBox()
        self._match.setStyleSheet("QComboBox { combobox-popup: 0; }")
        self._match.setMaxVisibleItems(10)
        self._match.activated[str].connect(self.on_match_changed)
        match_layout.addWidget(self._match)
        self.hide_match()
        main_layout.addLayout(match_layout)
        # Team
        team_layout = QHBoxLayout()
        self._team_label = QLabel("Team:")
        team_layout.addWidget(self._team_label)
        self._team_group = QButtonGroup()
        self._home_team = QRadioButton("Home")
        self._away_team = QRadioButton("Away")
        self._team_group.addButton(self._home_team)
        self._team_group.addButton(self._away_team)
        self._home_team.toggled.connect(self.on_clicked_team)
        self._away_team.toggled.connect(self.on_clicked_team)
        team_layout.addWidget(self._home_team)
        team_layout.addWidget(self._away_team)
        self.hide_team()
        main_layout.addLayout(team_layout)
        # Player
        player_layout = QHBoxLayout()
        self._player_label = QLabel("Player:")
        player_layout.addWidget(self._player_label)
        self._player = QComboBox()
        self._player.setStyleSheet("QComboBox { combobox-popup: 0; }")
        self._player.activated[str].connect(self.on_player_changed)
        self._player.setMinimumWidth(300)
        player_layout.addWidget(self._player)
        self.hide_player()
        main_layout.addLayout(player_layout)
        # Add a toolbar for browsing between plots
        self._toolbar = QToolBar()
        self._plot_passes_action = QAction("Passes", self._toolbar)
        self._plot_passes_action.triggered.connect(self.plot_passes)
        self._plot_passes_action.triggered.connect(self.add_passes_layer)
        self._plot_shots_action = QAction("Shots", self._toolbar)
        self._plot_shots_action.triggered.connect(self.plot_shots)
        self._plot_shots_action.triggered.connect(self.add_shots_layer)
        self._plot_heatmap_action = QAction("Heatmap", self._toolbar)
        self._plot_heatmap_action.triggered.connect(self.plot_heatmap)
        self._toolbar.addAction(self._plot_passes_action)
        self._toolbar.addAction(self._plot_shots_action)
        self._toolbar.addAction(self._plot_heatmap_action)
        self._toolbar.setEnabled(False)
        main_layout.addWidget(self._toolbar)
        # Add the pitch as a specific widget
        self.__pitch = SoccerPitchWidget(parent)
        self.__layer_tree_root = QgsProject.instance().layerTreeRoot()
        main_layout.addWidget(self.__pitch)

    def hide_match(self):
        """
        Hide and disable all match layout elements
        """
        self._match.setEnabled(False)
        self._match_label.hide()
        self._match.hide()

    def show_match(self):
        """
        Show and enable all match layout elements
        """
        self._match_label.show()
        self._match.show()
        self._match.setEnabled(True)
        self._match.setCurrentIndex(-1)

    def hide_team(self):
        """
        Hide and disable all team layout elements
        """
        self.reset_team()
        self._home_team.setEnabled(False)
        self._away_team.setEnabled(False)
        self._team_label.hide()
        self._home_team.hide()
        self._away_team.hide()

    def reset_team(self):
        """
        Reset checked radiobuttons for teams
        """
        self._team_group.setExclusive(False)
        self._home_team.setChecked(False)
        self._away_team.setChecked(False)
        self._team_group.setExclusive(True)

    def show_team(self):
        """
        Show and enable all team layout elements
        """
        self.reset_team()
        self._team_label.show()
        self._home_team.show()
        self._away_team.show()
        self._home_team.setEnabled(True)
        self._away_team.setEnabled(True)

    def hide_player(self):
        """
        Hide and disable all player layout elements
        """
        self._player.setEnabled(False)
        self._player_label.hide()
        self._player.hide()

    def show_player(self):
        """
        Show and enable all player layout elements
        """
        self._player_label.show()
        self._player.show()
        self._player.setEnabled(True)
        self._player.setCurrentIndex(-1)

    def on_competition_changed(self, text):
        # Quite ugly string management for retrieving the competition...
        compet_name, season_name = text[:-1].split(" (")
        self.compet_id, self.season_id = [
            (c["competition_id"], c["season_id"])
            for c in self.__dataset.competitions
            if c["competition_name"] == compet_name and c["season_name"] == season_name
        ][0]
        self.__dataset.load_matchs(self.compet_id, self.season_id)
        self.log(
            message=f"DEBUG - compet id: {self.compet_id} | season_id: {self.season_id}",
            log_level=4,
        )
        self._match.clear()
        self._player.clear()
        self._match.addItems(
            [
                f"{m['home_team']['home_team_name']} vs. {m['away_team']['away_team_name']}"
                for m in self.__dataset.matchs[self.compet_id][self.season_id]
            ]
        )
        self.show_match()
        self.hide_team()
        self.hide_player()
        self._toolbar.setEnabled(False)
        self._home_team.setText("Home")
        self._away_team.setText("Away")

    def on_match_changed(self, text):
        home_team, away_team = text.split(" vs. ")
        self._home_team.setText(home_team)
        self._away_team.setText(away_team)
        # Need to hide team to reset checked radiobutton
        self.hide_team()
        self.hide_player()
        self.match_id = [
            m["match_id"]
            for m in self.__dataset.matchs[self.compet_id][self.season_id]
            if m["home_team"]["home_team_name"] == home_team
            and m["away_team"]["away_team_name"] == away_team
        ][0]
        self.log(message=f"DEBUG - match id: {self.match_id}", log_level=4)
        self.__dataset.load_events(self.match_id)
        self.__dataset.load_lineups(self.match_id)
        self._toolbar.setEnabled(False)
        self.show_team()

    def on_player_changed(self, text):
        self.player_id = [
            p["player_id"]
            for p in self.__dataset.lineups[self.match_id][self.team_id]["lineup"]
            if p["player_name"] == text
        ][0]
        self._toolbar.setEnabled(True)

    def on_clicked_team(self):
        if self._home_team.isChecked():
            self.team_id = [
                idx
                for idx, t in enumerate(self.__dataset.lineups[self.match_id])
                if t["team_name"] == self._home_team.text()
            ][0]
            self.log(
                message=f"DEBUG - {self._home_team.text()} is selected {self.team_id}",
                log_level=4,
            )
        elif self._away_team.isChecked():
            self.team_id = [
                idx
                for idx, t in enumerate(self.__dataset.lineups[self.match_id])
                if t["team_name"] == self._away_team.text()
            ][0]
            self.log(
                message=f"DEBUG -{self._away_team.text()} is selected {self.team_id}",
                log_level=4,
            )
        self._player.clear()
        self._player.addItems(
            [
                p["player_name"]
                for p in self.__dataset.lineups[self.match_id][self.team_id]["lineup"]
            ]
        )
        self._toolbar.setEnabled(False)
        self.show_player()

    def plot_passes(self):
        """Plot passes (using mplsoccer pitch) done by selected player in dialog."""
        self.log(message="DEBUG - Plot passes", log_level=4)
        pass_events = [
            {
                "coordinates_x": e["location"][0],
                "coordinates_y": e["location"][1],
                "end_coordinates_x": e["pass"]["end_location"][0],
                "end_coordinates_y": e["pass"]["end_location"][1],
            }
            for e in self.__dataset.events[self.match_id]
            if e["type"]["name"] == "Pass"
            and e["player"]["id"] == self.player_id
            and "outcome" not in e["pass"]
        ]
        pass_events = pd.DataFrame(pass_events)
        if pass_events.shape[0] > 0:
            self.__pitch.plot("passes", pass_events)
        else:
            self.log(message="No pass!", log_level=4)

    def plot_shots(self):
        """Plot shots (using mplsoccer pitch) done by selected player in dialog."""
        self.log(message="DEBUG - Plot shots", log_level=4)
        shot_events = [
            {
                "coordinates_x": e["location"][0],
                "coordinates_y": e["location"][1],
                "end_coordinates_x": e["shot"]["end_location"][0],
                "end_coordinates_y": e["shot"]["end_location"][1],
            }
            for e in self.__dataset.events[self.match_id]
            if e["type"]["name"] == "Shot" and e["player"]["id"] == self.player_id
        ]
        shot_events = pd.DataFrame(shot_events)
        if shot_events.shape[0] > 0:
            self.__pitch.plot("shots", shot_events)
        else:
            self.log(message="No shot!", log_level=4)

    def plot_heatmap(self):
        """Plot heatmap (using mplsoccer pitch) for selected player in dialog."""
        self.log(message="DEBUG - Plot heatmap", log_level=4)
        pass_events = [
            {
                "coordinates_x": e["location"][0],
                "coordinates_y": e["location"][1],
                "end_coordinates_x": e["pass"]["end_location"][0],
                "end_coordinates_y": e["pass"]["end_location"][1],
            }
            for e in self.__dataset.events[self.match_id]
            if e["type"]["name"] == "Pass"
            and e["player"]["id"] == self.player_id
            and "outcome" not in e["pass"]
        ]
        pass_events = pd.DataFrame(pass_events)
        if pass_events.shape[0] > 0:
            self.__pitch.plot("heatmap", pass_events)
        else:
            self.log(message="DEBUG - No passes to draw heatmap", log_level=4)

    def add_passes_layer(self):
        """
        Add a new temporary layer in QGIS main canvas with all the passes
            done by selected player in dialog.
        Load symbology from a QML file.
        """
        self.log(message="DEBUG - Add passes layer", log_level=4)
        group_name = self._competition.currentText() + " " + self._match.currentText()
        layer_name = "Passes - " + self._player.currentText()
        match_group = self.__layer_tree_root.findGroup(group_name)
        if not match_group:
            match_group = QgsLayerTreeGroup(group_name)
            self.__layer_tree_root.insertChildNode(0, match_group)
        layer = QgsVectorLayer("LineString", layer_name, "memory")
        layer.setCrs(QgsCoordinateReferenceSystem("EPSG:3857"))
        dp = layer.dataProvider()
        dp.addAttributes(
            [
                QgsField("receiver", QVariant.String),
                QgsField("outcome", QVariant.String),
                QgsField("type", QVariant.String),
                QgsField("body_part", QVariant.String),
                QgsField("height", QVariant.String),
                QgsField("length", QVariant.Double),
                QgsField("shot_assist", QVariant.Bool),
                QgsField("goal_assist", QVariant.Bool),
            ]
        )
        layer.updateFields()
        # Feed QgsVectorLayer with all of the pass events that matches the selected player
        for e in self.__dataset.events[self.match_id]:
            if e["type"]["name"] == "Pass" and e["player"]["id"] == self.player_id:
                f = QgsFeature()
                f.setGeometry(
                    QgsGeometry.fromPolylineXY(
                        [
                            QgsPointXY(e["location"][0], -e["location"][1]),
                            QgsPointXY(
                                e["pass"]["end_location"][0],
                                -e["pass"]["end_location"][1],
                            ),
                        ]
                    )
                )
                f.setAttributes(
                    [
                        e.get("pass", {}).get("recipient", {}).get("name", None),
                        e.get("pass", {}).get("outcome", {}).get("name", None),
                        e.get("pass", {}).get("type", {}).get("name", None),
                        e.get("pass", {}).get("body_part", {}).get("name", None),
                        e.get("pass", {}).get("height", {}).get("name", None),
                        e.get("pass", {}).get("length", 0),
                        e.get("pass", {}).get("shot_assist", False),
                        e.get("pass", {}).get("goal_assist", False),
                    ]
                )
                dp.addFeature(f)

        layer.updateExtents()
        # Apply predefined style from QML file
        style_file_name = QGS_PROJECT_DIR.resolve().joinpath("styles/passes.qml")
        layer.loadNamedStyle(str(style_file_name))
        # Insert the layer at the first position in layer group corresponding to the selected match
        QgsProject.instance().addMapLayer(layer, False)
        match_group.insertLayer(0, layer)
        self.zoom_to_pitch()

    def add_shots_layer(self):
        """
        Add a new temporary layer in QGIS main canvas with all the shots
            done by selected player in dialog.
        Load symbology from a QML file.
        """
        self.log(message="DEBUG - Add shots layer", log_level=4)
        group_name = self._competition.currentText() + " " + self._match.currentText()
        layer_name = "Shots - " + self._player.currentText()
        match_group = self.__layer_tree_root.findGroup(group_name)
        if not match_group:
            match_group = QgsLayerTreeGroup(group_name)
            self.__layer_tree_root.insertChildNode(0, match_group)
        layer = QgsVectorLayer("LineString", layer_name, "memory")
        layer.setCrs(QgsCoordinateReferenceSystem("EPSG:3857"))
        dp = layer.dataProvider()
        dp.addAttributes(
            [
                QgsField("xg", QVariant.Double),
                QgsField("outcome", QVariant.String),
                QgsField("type", QVariant.String),
                QgsField("technique", QVariant.String),
                QgsField("body_part", QVariant.String),
                QgsField("aerial_won", QVariant.Bool),
                QgsField("follows_dribble", QVariant.Bool),
                QgsField("first_time", QVariant.Bool),
                QgsField("deflected", QVariant.Bool),
            ]
        )
        layer.updateFields()
        # Feed QgsVectorLayer with all of the shot events that matches the selected player
        for e in self.__dataset.events[self.match_id]:
            if e["type"]["name"] == "Shot" and e["player"]["id"] == self.player_id:
                f = QgsFeature()
                f.setGeometry(
                    QgsGeometry.fromPolylineXY(
                        [
                            QgsPointXY(e["location"][0], -e["location"][1]),
                            QgsPointXY(
                                e["shot"]["end_location"][0],
                                -e["shot"]["end_location"][1],
                            ),
                        ]
                    )
                )
                f.setAttributes(
                    [
                        e.get("shot", {}).get("statsbomb_xg", 0),
                        e.get("shot", {}).get("outcome", {}).get("name", None),
                        e.get("shot", {}).get("type", {}).get("name", None),
                        e.get("shot", {}).get("technique", {}).get("name", None),
                        e.get("shot", {}).get("body_part", {}).get("name", None),
                        e.get("shot", {}).get("aerial_won", False),
                        e.get("shot", {}).get("follows_dribble", False),
                        e.get("shot", {}).get("first_time", False),
                        e.get("shot", {}).get("deflected", False),
                    ]
                )
                dp.addFeature(f)

        layer.updateExtents()
        # Apply predefined style from QML file
        style_file_name = QGS_PROJECT_DIR.resolve().joinpath("styles/shots_xg.qml")
        layer.loadNamedStyle(str(style_file_name))
        # Insert the layer at the first position in layer group corresponding to the selected match
        QgsProject.instance().addMapLayer(layer, False)
        match_group.insertLayer(0, layer)
        self.zoom_to_pitch()

    def zoom_to_pitch(self):
        """Zoom to group layer that represents soccer pitch."""
        extent = QgsRectangle()
        extent.setMinimal()

        group = self.__layer_tree_root.findGroup("Soccer pitch")
        for child in group.children():
            if isinstance(child, QgsLayerTreeLayer):
                extent.combineExtentWith(child.layer().extent())
        self.__iface.mapCanvas().setExtent(extent)
