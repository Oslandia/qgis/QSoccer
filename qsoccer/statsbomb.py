"""Load football opendata through Kloppy API. For instance, one focuses on Statsbomb data.

For more details, see https://github.com/statsbomb/open-data.
"""

import json
import logging
import os
from pathlib import Path

from qgis.core import QgsFileDownloader
from qgis.PyQt.QtCore import QEventLoop, QUrl

logger = logging.getLogger(__name__)

GITHUB_URL_PREFIX = "https://raw.githubusercontent.com/statsbomb/open-data/master/"

CURDIR = Path(os.path.dirname(os.path.realpath(__file__)))


def download_file(url, filename):
    """Load a .json located at the given url, and store it to the file system

    Parameters
    ----------
    url : str
        URL of the data
    filename : pathlib.Path
        Path where must be written the data on the file system
    """
    loop = QEventLoop()
    downloader = QgsFileDownloader(
        url=QUrl(url), outputFileName=str(filename), delayStart=True
    )
    downloader.downloadExited.connect(loop.quit)
    downloader.startDownload()
    loop.exec_()


class StatsbombDataset:
    """Load Statsbomb opendata through Kloppy"""

    def __init__(self):
        self.data = None
        self.data_folder = CURDIR / "statsbomb_data"
        self.data_folder.mkdir(exist_ok=True, parents=True)
        self.matchs = dict()
        self.events = dict()
        self.lineups = dict()

    def competition_url(self):
        return GITHUB_URL_PREFIX + "data/competitions.json"

    def match_url(self, competition_id, season_id):
        return GITHUB_URL_PREFIX + f"data/matches/{competition_id}/{season_id}.json"

    def event_url(self, match_id):
        return GITHUB_URL_PREFIX + f"data/events/{match_id}.json"

    def lineup_url(self, match_id):
        return GITHUB_URL_PREFIX + f"data/lineups/{match_id}.json"

    def load_competitions(self):
        competition_filepath = self.data_folder / "competition.json"
        if not competition_filepath.exists():
            logger.info("Download competition data...")
            download_file(self.competition_url(), competition_filepath)
        with open(competition_filepath) as fobj:
            self.competitions = json.load(fobj)
            # Initialize match dictionnary for each available competition
            for compet in self.competitions:
                self.matchs[compet["competition_id"]] = dict()

    def load_matchs(self, competition_id, season_id):
        match_filepath = (
            self.data_folder / "matches" / f"{competition_id}" / f"{season_id}.json"
        )
        if not match_filepath.exists():
            logger.info(
                "Download match data for %s (season %s)...", competition_id, season_id
            )
            match_filepath.parent.mkdir(exist_ok=True, parents=True)
            download_file(
                self.match_url(competition_id, season_id),
                match_filepath,
            )
        with open(match_filepath) as fobj:
            self.matchs[competition_id][season_id] = json.load(fobj)

    def load_events(self, match_id):
        event_filepath = self.data_folder / "events" / f"{match_id}.json"
        if not event_filepath.exists():
            logger.info("Download event data for match %s...", match_id)
            event_filepath.parent.mkdir(exist_ok=True, parents=True)
            download_file(self.event_url(match_id), event_filepath)
        with open(event_filepath) as fobj:
            self.events[match_id] = json.load(fobj)

    def load_lineups(self, match_id):
        lineup_filepath = self.data_folder / "lineups" / f"{match_id}.json"
        if not lineup_filepath.exists():
            logger.info("Download lineup data for match %s...", match_id)
            lineup_filepath.parent.mkdir(exist_ok=True, parents=True)
            download_file(
                self.lineup_url(match_id),
                self.data_folder / "lineups" / f"{match_id}.json",
            )
        with open(lineup_filepath) as fobj:
            self.lineups[match_id] = json.load(fobj)
