# Credits and sponsoring

This plugin has been developed by Oslandia (<http://www.oslandia.com>). Oslandia provides support and assistance for QGIS and associated tools, including this plugin.

## Acknowledgement

The plugin uses data from [Statsbomb](https://statsbomb.com/). Interested users may explore the [Github project](https://github.com/statsbomb/open-data) where demonstration data is provided.

![Statsbomb_logo](/_static/logo_statsbomb_regular.png)

## Funding

```{admonition} Ready to contribute?
Plugin is free to use, not to develop. If you use it quite intensively or are interested in improving it, please consider to contribute to the code, to the documentation or fund some developments:

Want to fund? Please, [send us an email](mailto:qgis@oslandia.com)
```
