# {{ title }} - Documentation

> **Description :** {{ description }}  
> **Author and contributors:** {{ author }}  
> **Plugin version:** {{ version }}  
> **QGIS minimum version:** {{ qgis_version_min }}  
> **QGIS maximum version:** {{ qgis_version_max }}  
> **Source code:** {{ repo_url }}  
> **Last documentation update:** {{ date_update }}

`QSoccer` aims at depicting individual player actions through a QGIS Widget. In order to localize the team on the map, one highlights the involved teams in the main canvas.

For now, the plugin shows :
* pass maps (every pass made by any player during a match):

    ![Pass map](/_static/passmap_example.png)

* shots maps (every shot made by any player during a match):

    ![Shots map](/_static/shotsmap_example.png)

* heatmaps (area of action of a player during a match):

    ![Heatmap](/_static/heatmap_example.png)

----

```{toctree}
---
caption: User guide
maxdepth: 1
---
installation
```

```{toctree}
---
caption: Contribution guide
maxdepth: 1
---
development/contribute
development/environment
development/documentation
development/packaging
development/testing
development/history
misc/credits
```
