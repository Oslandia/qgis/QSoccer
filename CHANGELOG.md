# CHANGELOG

The format is based on [Keep a Changelog](https://keepachangelog.com/), and this project adheres to [Semantic Versioning](https://semver.org/).

<!--

Unreleased

## version_tag - YYYY-DD-mm

### Added

### Changed

### Removed

-->

## 1.1.1 - 2022-10-26

- fix missing embedded dependency: seaborn

## 1.1.0 - 2022-10-06

- package embedded dependencies with plugin to avoid additional pip install
- do not crash if dependencies are not staisfied but warn the end-user
- extend tests
- update dependencies
- CI/CD: run git hooks against MR

## 1.0.0 - 2021-10-01

- First stable version for FOSS4G talk
- CHANGE: Use QgsFileDownloader (#12)
- FIX: the default demonstration project by introduction a .gpkg for the team layer. (#14)

## 0.2.0 - 2021-09-15

- Improve widget
- Better UI

## 0.1.0 - 2021-06-01

- First release
- Generated with the [QGIS Plugins templater](https://oslandia.gitlab.io/qgis/template-qgis-plugin/)
